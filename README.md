### Run
```
GO111MODULE=on go build
```
### Load Test
```
cd loadtest
k6 run --vus 500 --iterations 100000 load.js
```