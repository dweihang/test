import http from "k6/http";
import {
    check
} from 'k6';

export default function () {
    let res = http.get("http://localhost:5699/example/test2")
    check(res, {
        'is ok': (r) => r.status == 200,
    })
}