package main

import (
	"context"
	"github.com/gin-gonic/gin"
	"github.com/opentracing/opentracing-go"
	"github.com/raymond852/ginx"
	"github.com/raymond852/koncurrent/v3"
	"net/http"
)

type exampleAPIIF interface {
	ginx.API
	JSONTest(ctx *gin.Context)
}

func NewExampleAPI() exampleAPIIF {
	return exampleAPI{}
}

type exampleAPI struct {
}

var pe = koncurrent.NewPoolExecutor(100, 10)

func (e exampleAPI) RouteGroup() *ginx.RouteGroup {
	docTag := "example"
	rg := ginx.NewRouteGroup("/example")

	rg.
		Get("/test1").
		To(e.JSONTest).
		Doc().
		Summary("json test").
		Description("json test description").
		Tag(docTag).
		Response("200", ginx.JSONResponseBody(map[string]interface{}{}), "success")

	rg.
		Get("/test2").
		To(e.JSONTest2).
		Doc().
		Summary("json test").
		Description("json test description").
		Tag(docTag).
		Response("200", ginx.JSONResponseBody(map[string]interface{}{}), "success")

	return rg
}

func (e exampleAPI) JSONTest(ctx *gin.Context) {
	_, err := GetOne(ctx.Request.Context())
	if err != nil {
		_ = ctx.Error(err)
	} else {
		result2, err2 := GetOne(ctx.Request.Context())
		if err2 != nil {
			_ = ctx.Error(err)
		} else {
			ctx.AbortWithStatusJSON(http.StatusOK, result2)
		}
	}

}

func (e exampleAPI) JSONTest2(ctx *gin.Context) {
	if span := opentracing.SpanFromContext(ctx.Request.Context()); span != nil {
		span.SetOperationName("json test 2 API")
	}
	var result map[string]interface{}
	var dbTask koncurrent.TaskFunc = func(ctx context.Context) error {
		r, err := GetOne(ctx)
		result = r
		return err
	}

	var dbTask2 koncurrent.TaskFunc = func(ctx context.Context) error {
		r, err := GetOne(ctx)
		result = r
		return err
	}
	_, err := koncurrent.ExecuteParallel(
		dbTask.Pool(pe).Tracing("dbTask1").Recover(),
		dbTask2.Pool(pe).Tracing("dbTask2").Recover()).
		Await(ctx.Request.Context())
	if err != nil {
		_ = ctx.Error(err)
	} else {
		ctx.AbortWithStatusJSON(http.StatusOK, result)
	}
}
