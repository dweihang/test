package main

import (
	"fmt"
	jaegercfg "github.com/uber/jaeger-client-go/config"
	"io"
	"os"
)

func InitTracing() io.Closer {
	_ = os.Setenv("JAEGER_SERVICE_NAME", "test-service")
	_ = os.Setenv("JAEGER_AGENT_HOST", "localhost")
	_ = os.Setenv("JAEGER_SAMPLER_TYPE", "const")
	_ = os.Setenv("JAEGER_SAMPLER_PARAM", "1")
	_ = os.Setenv("JAEGER_REPORTER_LOG_SPANS", "true")

	cfg, err := jaegercfg.FromEnv()
	if err != nil {
		fmt.Printf("Could not parse Jaeger env vars: %s", err.Error())
		return nil
	}

	closer, err := cfg.InitGlobalTracer("test service")
	if err != nil {
		fmt.Printf("Could not initialize jaeger tracer: %s", err.Error())
		return nil
	}
	return closer
}
