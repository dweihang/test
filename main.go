package main

import (
	"github.com/gin-gonic/gin"
	"github.com/opentracing/opentracing-go"
	"github.com/raymond852/ginx"
	"net/http"
)

func main()  {
	InitTracing()
	err := InitMongo()
	if err != nil {
		println(err)
	}
	g := gin.New()
	ginx.Init("example description", "1.0.0", "An example title")
	ginx.UseSwaggerUI(g, "/apidoc")
	ginx.UseValidator(g, func(ctx *gin.Context, err error) {
		if err != nil {
			ctx.AbortWithStatusJSON(http.StatusBadRequest, map[string]interface{}{
				"error": err.Error(),
			})
		}
	})
	ginx.UseOpenTracing(g, opentracing.GlobalTracer())
	ginx.AddAPI(g, NewExampleAPI())


	srv := &http.Server{
		Addr:    ":5699",
		Handler: g,
	}
	_ = srv.ListenAndServe()
}
