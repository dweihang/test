package main

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"time"
)

var client *mongo.Client

func InitMongo() error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	cli, err := mongo.Connect(ctx, options.Client().ApplyURI("mongodb://localhost:27017"))
	client = cli
	return err
}

func GetOne(ctx context.Context) (map[string]interface{}, error) {
	collection := client.Database("local").Collection("startup_log")
	m := make(map[string]interface{})
	err := collection.FindOne(ctx, bson.M{}).Decode(m)
	return m, err
}
