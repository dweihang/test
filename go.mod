module github.com/raymond852/test

go 1.16

require (
	github.com/HdrHistogram/hdrhistogram-go v1.1.2 // indirect
	github.com/gin-gonic/gin v1.7.4
	github.com/opentracing/opentracing-go v1.2.0
	github.com/raymond852/ginx v1.1.0
	github.com/raymond852/koncurrent/v3 v3.1.0
	github.com/uber/jaeger-client-go v2.29.1+incompatible
	github.com/uber/jaeger-lib v2.4.1+incompatible // indirect
	go.mongodb.org/mongo-driver v1.7.3
	go.uber.org/atomic v1.9.0 // indirect
)
